<footer class="mt-4">
    <div class="bg-secondary">
        <div class="container">
            <div class="card-body">
                <div class="row align-items-center justify-content-center">
                    <div class="col-12 col-lg-3 px-md-0 mb-2 mb-lg-0 text-center text-lg-left">
                        <a style="font-family: 'Nunito-ExtraBold';" class="navbar-brand mr-auto no-select text-muted disabled footer-brand">
                            <i class="fas fa-graduation-cap"></i>eCOURSE
                        </a>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="row">
                            <div class="col-12 col-lg-4 mb-2 mb-lg-0 text-center text-lg-left">
                                <h4 class="text-muted">Contact</h4>
                                <span class="text-muted">rubichandrap@gmail.com</span>
                            </div>
                            <div class="col-12 col-lg-4 mb-2 mb-lg-0 text-center text-lg-left">
                                <h4 class="text-muted">Made For</h4>
                                <span class="text-muted">Own Purposes</span>
                            </div>
                            <div class="col-12 col-lg-4 mb-2 mb-lg-0 text-center text-lg-left">
                                <h4 class="text-muted">License</h4>
                                <span class="text-muted">MIT License &copy; 2020 Rubi Chandraputra</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>