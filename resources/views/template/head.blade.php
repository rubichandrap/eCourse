    <!-- META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Rubi Chandraputra">

    <!-- CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- css argon -->
    <link rel="stylesheet" href="{{ asset('asset/argon/css/argon-dashboard.min.css') }}">
    <!-- css style -->
    <link rel="stylesheet" href="{{ asset('asset/main/css/style.css') }}">
    <!-- css nucleo -->
    <link rel="stylesheet" href="{{ asset('asset/argon/js/plugins/nucleo/css/nucleo.css') }}">
    <!-- css cololib -->
    <link href="{{ asset('asset/colorlib/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/colorlib/fonts/css/material-design-iconic-font.css') }}" rel="stylesheet">
    <!-- css fontawesome -->
    <link rel="stylesheet" href="{{ asset('asset/fontawesome-free-5.12.0/css/all.min.css') }}">
    <!-- css bootstrap datatables -->
    <link rel="stylesheet" href="{{ asset('asset/main/css/dataTables.bootstrap.min.css') }}">
    <!-- css bootstrap responsive -->
    <link rel="stylesheet" href="{{ asset('asset/main/css/responsive.bootstrap.min.css') }}">

    <!-- ICON -->
    <link href="{{ asset('asset/main/images/icon.png') }}" rel="icon" type="image/x-ico">

    <!-- PROJEK INI MERUPAKAN TUGAS AKHIR SEBAGAI SYARAT KELULUSAN JENJANG STRATA-1 PADA STMIK MUHAMMADIYAH JAKARTA -->
    <!-- DESAIN PROJEK INI TERINSPIRASI DARI WWW.DICODING.COM, BACK-END MERUPAKAN IMPLEMENTASI DAN PENYEMPURNAAN 
    DARI RISET DAN PROJEK-PROJEK SAYA SEBELUMNYA ANTARA LAIN WWW.AKPA.NET.ID DAN WWW.OMAHNGAYOMI.NAUFALIBNUSALAM.COM -->

    <!-- RUBI CHANDRAPUTRA, 2020 -->