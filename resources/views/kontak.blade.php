@extends('layouts.app')

@section('content')
<header>

</header>
<article class="card-post my-5 no-select">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8 text-center">
                <div class="card border-0">
                    <div class="card-body">
                        <h3 class="text-muted">
                            Kirim saran dan temuan bug beserta screenshot ke rubichandrap@gmail.com
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
@endsection