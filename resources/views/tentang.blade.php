@extends('layouts.app')

@section('content')
<header>

</header>
<article class="card-post my-5 no-select">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8 text-center">
                <div class="card border-0">
                    <div class="card-body">
                        <div>
                            <a class="btn bg-transparent shadow-none" href="#">
                                <img src="{{ asset('asset/main/images/rubi.jpg') }}" class="rounded-circle" width="160" height="160">
                            </a>
                        </div>
                        <div class="pt-2">
                            <div>
                                <h2 class="mb-0">
                                    Rubi Chandraputra
                                </h2>
                                <p class="text-muted">
                                    rubichandrap@gmail.com
                                </p>
                                <p class="text-muted mb-0">
                                    Portofolios:
                                </p>
                                <div>
                                    <a href="https://www.akpa.net.id">akpa.net.id</a>
                                    <span class="mx-2">|</span>
                                    <a href="http://www.omahngayomi.naufalibnusalam.com">omahngayomi</a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div>
                            <p>
                                <span class="mr-2" style="font-size: 2rem;"><i class="fas fa-quote-left"></i></span><span class="font-weight-bold">eCourse</span>, merupakan sistem pembelajaran <i>online</i> atau yang
                                dikenal sebagai <i>E-Learning</i>, yang dibuat khusus bagi keluarga STMIK Muhammadiyah Jakarta.
                                Projek ini merupakan tugas akhir dan produk murni yang diciptakan untuk memenuhi syarat kelulusan jenjang
                                Strata-1 pada STMIK Muhammadiyah Jakarta.
                            </p>
                            <p>
                                <span class="font-weight-bold">eCourse</span> memiliki sistem yang layak untuk diimplementasikan
                                ke dalam suatu sistem pembelajaran <i>online</i> karena memiliki kompleksitas yang cukup tinggi. Projek ini
                                merupakan sebuah penyempurnaan dari beberapa projek saya sebelumnya, seperti website <i>Internet Service Provider</i>
                                <a href="https://www.akpa.net.id">AKPA</a> dan website yang merupakan produk dari lomba Jakbee 2019
                                <a href="http://www.omahngayomi.naufalibnusalam.com"> omahngayomi</a>, dan <span class="font-weight-bold">100% Made From Scratch</span>.
                            </p>
                            <p>
                                Saya mengucapkan <span class="font-weight-bold">Ribuan Terima Kasih</span> kepada orang-orang yang
                                telah mendukung saya dari segi mental dan ilmu. Terima Kasih kepada adik dan kedua orang tua, karena
                                tidak henti-hentinya memberikan semangat. Pujaan hati, Riska Alfiani <span class="text-red"><i class="fas fa-heart"></i>
                                </span> . Teman baik saya Naufal Ibnu Salam, ngoding bareng sama lo memang
                                momen cari ilmu paling luar biasa! <span class="font-weight-bold text-muted">jangan lupa projek vue js fal! </span>
                                <span class="text-green"><i class="fab fa-vuejs"></i></span> . Dan semua orang yang tidak bisa saya sebutkan satu persatu.
                            </p>
                            <p>
                                Do'a dan harapan saya, agar sistem <i>E-Learning</i> ini dapat diimplementasikan ke dalam sistem yang sudah berjalan
                                pada STMIK Muhammadiyah Jakarta. Bagi saya, tidak ada penghargaan dan apresiasi yang lebih baik dari pihak kampus kepada mahasiswa,
                                melainkan memberdayakan produk-produk ciptaan kami. Terima kasih para dosen. Semoga STMIK Muhammadiyah Jakarta dapat
                                terus berkembang detik demi detik. Aamiin! <span class="ml-2" style="font-size: 2rem;"><i class="fas fa-quote-right"></i></span>
                            </p>
                        </div>
                        <hr>
                        <div>
                            <span class="text-muted"><i>~ Rubi Chandraputra, 2020</i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
@endsection